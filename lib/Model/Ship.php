<?php

class Ship {
  private $id;
  private $name;
  private $weaponPower = 0;
  private $jediFactor = 0;
  private $strenght = 0;
  private $underRepair;

  public function __construct($name) {
    $this->name = $name;
    $this->underRepair = mt_rand(1, 100) < 30;
  }

  public function isFunctional() {
    return !$this->underRepair;
  }

  public function sayHello() {
    echo 'Hello!';
  }

  public function getName() {
    return $this->name;
  }

  public function getNameAndSpecs($useShortFormat = false) {
    if($useShortFormat) {
      return sprintf(
        '%s: %s/%s/%s',
        $this->name,
        $this->weaponPower,
        $this->jediFactor,
        $this->strenght
      );
    } else {
      return sprintf(
        '%s: w:%s, j:%s, s:%s',
        $this->name,
        $this->weaponPower,
        $this->jediFactor,
        $this->strenght
      );
    }
  }

  public function doesGivenShipHaveMoreStrenght($givenShip) {
    return $givenShip->strenght > $this->strenght;
  }

  public function setStrenght($strenght) {
    if(!is_numeric($strenght)) {
      throw new Exception('Invalid strenght passed' . $strenght);
    }
    $this->strenght = $strenght;
  }

  public function getStrenght() {
    return $this->strenght;
  }

  /**
   * @return int
   */
  public function getWeaponPower()
  {
    return $this->weaponPower;
  }

  /**
   * @param int $weaponPower
   */
  public function setWeaponPower($weaponPower)
  {
    $this->weaponPower = $weaponPower;
  }

  /**
   * @return int
   */
  public function getJediFactor()
  {
    return $this->jediFactor;
  }

  /**
   * @param int $jediFactor
   */
  public function setJediFactor($jediFactor)
  {
    $this->jediFactor = $jediFactor;
  }

  /**
   * @param mixed $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return mixed
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param integer $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

}